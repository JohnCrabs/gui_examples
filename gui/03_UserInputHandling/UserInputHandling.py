import tkinter as tk


def CreateWindow():
    # Create the main window
    root = tk.Tk()
    root.title("User Intput Handling")
    # ------ START BUILDING THE APP ------

    label_Hello = tk.Label(text="")
    entry_LineEdit = tk.Entry(root)

    def action_Ok():
        label_Hello.config(text=entry_LineEdit.get())
    def action_Cancel():
        root.destroy()

    button_Ok = tk.Button(root, text="Ok", padx=25, command=action_Ok)
    button_Cancel = tk.Button(root, text="Cancel", padx=25, command=action_Cancel)

    entry_LineEdit.grid(row=0, column=1)
    label_Hello.grid(row=0, column=0)
    button_Ok.grid(row=1, column=0)
    button_Cancel.grid(row=1, column=1)

    # ------ END OF CODE SECTION / START EXEC LOOP ------
    # The MainLoop
    root.mainloop()

if __name__ == "__main__":
    CreateWindow()
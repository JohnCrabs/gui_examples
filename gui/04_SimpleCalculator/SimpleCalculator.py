import tkinter as tk

_BUTTON_SIZE = 40
_DEFAULT_NUMBER = "0.00"
class SimpleCalculator():
    def __init__(self):
        # Create the main window
        self.root = tk.Tk()
        self.root.title("Simple Calculator")

        self.entry_LineText = tk.Entry(self.root, width=35, borderwidth=5, font="15")

        # --------------------------------
        # Utils Variables
        self.textNumber = _DEFAULT_NUMBER
        self.mathCommand = ""
        self.numberList = []
        self.answer = ""


        # --------------------------------
        # Create Buttons
        self.button_1 = tk.Button(self.root, text="1", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("1"))
        self.button_2 = tk.Button(self.root, text="2", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("2"))
        self.button_3 = tk.Button(self.root, text="3", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("3"))
        self.button_4 = tk.Button(self.root, text="4", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("4"))
        self.button_5 = tk.Button(self.root, text="5", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("5"))
        self.button_6 = tk.Button(self.root, text="6", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("6"))
        self.button_7 = tk.Button(self.root, text="7", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("7"))
        self.button_8 = tk.Button(self.root, text="8", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("8"))
        self.button_9 = tk.Button(self.root, text="9", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("9"))
        self.button_0 = tk.Button(self.root, text="0", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE/2, font="sans 12",
                                  command=lambda: self.action_clicked("0"))
        self.button_dot = tk.Button(self.root, text=".", padx=_BUTTON_SIZE,
                                  pady=_BUTTON_SIZE / 2, font="sans 12",
                                  command=lambda: self.action_clicked("."))

        self.button_plus = tk.Button(self.root, text="+", padx=_BUTTON_SIZE,
                                     pady=_BUTTON_SIZE/2, font="sans 12",
                                     command=lambda: self.action_clicked("+"))
        self.button_minus = tk.Button(self.root, text="-", padx=_BUTTON_SIZE,
                                      pady=_BUTTON_SIZE/2, font="sans 12",
                                      command=lambda: self.action_clicked("-"))
        self.button_mul = tk.Button(self.root, text="*", padx=_BUTTON_SIZE,
                                    pady=_BUTTON_SIZE/2, font="sans 12",
                                    command=lambda: self.action_clicked("*"))
        self.button_div = tk.Button(self.root, text="/", padx=_BUTTON_SIZE,
                                    pady=_BUTTON_SIZE/2, font="sans 12",
                                    command=lambda: self.action_clicked("/"))
        self.button_clear = tk.Button(self.root, text="CLEAR", padx=_BUTTON_SIZE,
                                      pady=_BUTTON_SIZE/2,  fg="red", font="sans 8 bold",
                                      command=lambda: self.action_clicked("C"))
        self.button_enter = tk.Button(self.root, text="=", padx=_BUTTON_SIZE,
                                      pady=_BUTTON_SIZE/2, font="sans 12",
                                      command=lambda: self.action_clicked("="))

        # Add to grid
        self.entry_LineText.grid(row=0, column=0, columnspan=4, padx=10, pady=10)
        self.entry_LineText.insert(0, self.textNumber)

        self.button_clear.grid(row=1, column=0, columnspan=4)

        self.button_7.grid(row=2, column=0)
        self.button_8.grid(row=2, column=1)
        self.button_9.grid(row=2, column=2)
        self.button_plus.grid(row=2, column=3)

        self.button_4.grid(row=3, column=0)
        self.button_5.grid(row=3, column=1)
        self.button_6.grid(row=3, column=2)
        self.button_minus.grid(row=3, column=3)

        self.button_1.grid(row=4, column=0)
        self.button_2.grid(row=4, column=1)
        self.button_3.grid(row=4, column=2)
        self.button_mul.grid(row=4, column=3)

        self.button_0.grid(row=5, column=0)
        self.button_dot.grid(row=5, column=1)
        self.button_enter.grid(row=5, column=2)
        self.button_div.grid(row=5, column=3)

        # ---------------------- END SECTION ----------- #
        # The MainLoop
        self.root.mainloop()

    def action_clicked(self, value):
        if '0' <= value <= '9' or value == '.':
            if self.textNumber == _DEFAULT_NUMBER:
                self.textNumber = str(value)
            else:
                self.textNumber += str(value)
            self.entry_LineText.delete(0, tk.END)
            self.entry_LineText.insert(0, self.textNumber)

        elif value == "C":
            self.textNumber = _DEFAULT_NUMBER
            self.entry_LineText.delete(0, tk.END)
            self.entry_LineText.insert(0, self.textNumber)

        elif value == '+' or value == '-' or value == '*' or value == '/' or value == '=':
            self.numberList.append(float(self.textNumber))
            if value != '=':
                self.mathCommand = value
                self.textNumber = _DEFAULT_NUMBER
                self.entry_LineText.delete(0, tk.END)
                self.entry_LineText.insert(0, self.textNumber)

                if self.numberList.__len__() >= 2:
                    number = float(self.textNumber)
                    if self.mathCommand == '+':
                        number = self.numberList[0] + self.numberList[1]
                    elif self.mathCommand == '-':
                        number = self.numberList[0] - self.numberList[1]
                    elif self.mathCommand == '*':
                        number = self.numberList[0] * self.numberList[1]
                    elif self.mathCommand == '/':
                        number = self.numberList[0] / self.numberList[1]

                    self.answer = str(number)
                    self.textNumber = _DEFAULT_NUMBER
                    self.numberList = [number]
                    self.entry_LineText.delete(0, tk.END)
                    self.entry_LineText.insert(0, self.textNumber)

            else:
                if self.numberList.__len__() >= 2:
                    number = float(self.textNumber)
                    if self.mathCommand == '+':
                        number = self.numberList[0] + self.numberList[1]
                    elif self.mathCommand == '-':
                        number = self.numberList[0] - self.numberList[1]
                    elif self.mathCommand == '*':
                        number = self.numberList[0] * self.numberList[1]
                    elif self.mathCommand == '/':
                        number = self.numberList[0] / self.numberList[1]

                    self.answer = str(number)
                    self.textNumber = self.answer
                    self.numberList = []
                    self.entry_LineText.delete(0, tk.END)
                    self.entry_LineText.insert(0, self.answer)


if __name__ == "__main__":
    cal = SimpleCalculator()
import tkinter as tk

def CreateWindow():
    # Create the main window
    root = tk.Tk()
    root.title("Introduction to Grid System")
    # ------ START BUILDING THE APP ------

    # Create a Label Widget
    label_Hello = tk.Label(root, text="Hello World!")
    label_Text = tk.Label(root, text="This is a Text Label!")

    # Pack it into the screen
    label_Hello.grid(row=0, column=0)
    label_Text.grid(row=1, column=0)

    # ------ END OF CODE SECTION / START EXEC LOOP ------
    # The MainLoop
    root.mainloop()

if __name__ == "__main__":
    CreateWindow()
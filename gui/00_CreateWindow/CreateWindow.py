import tkinter as tk

def CreateWindow():
    # Create the main window
    root = tk.Tk()
    root.title("Create a Window")
    # ------ START BUILDING THE APP ------

    # Create a Label Widget
    label_Hello = tk.Label(root, text="Hello World")

    # Pack it into the screen
    label_Hello.pack()

    # ------ END OF CODE SECTION / START EXEC LOOP ------
    # The MainLoop
    root.mainloop()

if __name__ == "__main__":
    CreateWindow()